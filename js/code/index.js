let counter = 0,
    secCounter = 57,
    mCounter = 59,
    /*
    counter : відраховуе мілісекунди. в 1 секунді 1000 мілісекунд, але відображатись
        ,на табло, буде десятки та сотні, а тисяца вже буде переноситись на секунду.
        Тобто 893 мілісекунд буде відображати 89 на лічильнику.
    secCounter : рахунок секунд;
    mCounter : рахунок хвилин;
    Зараз початковий рахунок (перший рахунок, ресет скидуе на 00:00:00) за для Демонстрації анімації 
    */
    intervalHandler,
    myInterval,
    flag = false,
    flagCounter = false ,
    flashСounter = false;


const get = id => document.getElementById(id);


const count = () => {
    if(flagCounter == false ){
        get("mls").innerHTML = `0${counter}`;
        get("sec").innerHTML = `0${secCounter}`;
        get("min").innerHTML = `0${mCounter}`;    
        counter++;
        if (counter > 9){
            get("mls").innerHTML = counter;
        }
        if (counter >= 99){
            counter = 0 ;
            get("sec").innerHTML = `0${secCounter}`;
            secCounter++
        }
        if (secCounter > 9){
            get("sec").innerHTML = secCounter;
        }
        if (secCounter >= 59){
            secCounter = 0 ;
            get("sec").innerHTML = secCounter;
            get("min").innerHTML = `0${mCounter}`;
            mCounter++
        }
        if (mCounter > 9){
            get("min").innerHTML = mCounter;
        }
        if ( mCounter === 60 && counter === 1 ){
            counter = 0;
            secCounter = 0 ;
            mCounter = 0 ;
            myInterval = setInterval(setflashing, 1000);
            function setflashing() {
                if(flashСounter === false){ 
                    get("mls").innerHTML = get("mls").innerHTML == "99" ? "--" : "99";           
                    get("sec").innerHTML = get("sec").innerHTML == "59" ? "--" : "59";
                    get("min").innerHTML = get("min").innerHTML == "59" ? "--" : "59";
                    //Анімація починаеться при 60 хвилин, 10 мілісекунд.
                    // На цифірблаті буде відображатись 59хв , 59 секунд та 990 мілісекунд
                    // так як через 1 мылысекунду буде вже 1 година, тоб то вже 4 порядок
                    // (хоча выходить що є неточнысть в 11 мілісекунд)
                }
                if(flashСounter === true){
                    return 
                }
            }
            return setflashing(),
                flagCounter = true;
        }  
    }
    return;
}

const resetCount = () => {
    counter = 0;
}

get("startButton").onclick = () => {
    if(!flag){
      intervalHandler = setInterval(count, 10);  
      flag = true;
      flashСounter = false;
    }
    document.body.classList.add("green");
    document.body.classList.remove("red");
    document.body.classList.remove("silver");
    document.body.classList.remove("pink");
    document.body.classList.remove("green-b");
}

get("stopButton").onclick = () => {
    clearInterval(intervalHandler);
    document.body.classList.add("red");
    document.body.classList.remove("green");
    document.body.classList.remove("silver");
    document.body.classList.remove("pink");
    document.body.classList.remove("green-b");
    flag = false
    flashСounter = true;
}

get("resetButton").onclick = () => {
    counter = 0;
    secCounter = 0;
    mCounter = 0;
    clearInterval(intervalHandler);
    clearInterval(myInterval);
    get("mls").innerHTML = `0${counter}`,
    get("sec").innerHTML = `0${secCounter}`,
    get("min").innerHTML = `0${mCounter}`;
    document.body.classList.add("silver");
    document.body.classList.remove("green");
    document.body.classList.remove("red");
    document.body.classList.remove("pink");
    document.body.classList.remove("green-b");
    flag = false;
    flagCounter = false;
    flashСounter = true;
}

///////////////////////////////////////////////////////////////////
let inputField ,
    inputError,
    buttonForCheck ;

inputField = document.createElement("input");
inputField.classList.add("input-field");
inputField.textContent = " ";
inputField.placeholder = "000-000-00-00";

buttonForCheck = document.createElement('button');
buttonForCheck.classList.add('button-check');
buttonForCheck.textContent = "Button";

inputError = document.createElement("span");
inputError.classList.add("input-field");
inputError.textContent = "";

document.getElementById('phaseTwo')
    .appendChild(inputError);
document.getElementById('phaseTwo')
    .appendChild(inputField);
document.getElementById('phaseTwo')
    .appendChild(buttonForCheck);

    
 buttonForCheck.onclick = function() {
    const pattern = /\d{3}-\d{3}-\d{2}-\d{2}/;
    if(pattern.test(inputField.value) === false){
        inputError.textContent = "Error, plz try again";
        document.body.classList.add("pink");
    }
    if(pattern.test(inputField.value) === true){
        document.body.classList.add("green-b");
        //document.location.href = 'https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg';
        inputError.textContent = "";
    }
    pattern.test(inputField.value)
    console.log(   );
 }

